package model.data_structures;

public class Node {
public Node sig;
public Node ant;

public Node darSiguiente()
{
	return sig;
}
public Node(Node pAnt, Node pSig)
{
	sig = pSig;
	ant = pAnt;
}
public Node darAnterior()
{
	return ant;
}
public void cambiarAnterior(Node pNodo)
{
	ant = pNodo;
	pNodo.cambiarSiguiente(this);
}
public void cambiarSiguiente(Node pNodo)
{
	sig = pNodo;
	pNodo.cambiarAnterior(this);
}
}
